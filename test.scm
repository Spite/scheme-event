(import (scheme base)
        (scheme write)
        (retropikzel event v0.1.0 main))




(event-handle
  'test
  (lambda (arg1)
    (display "Hello from test event!")
    (newline)
    (display arg1)
    (newline)))


(define main
  (lambda ()
    (event-push 'test (list 1))
    (event-update)
    (main)))

(main)
